# guia8_u3
*métodos de ordenamiento interno*

***Pre-requisitos***

- C++
- Make
- Ubuntu

***Instalación***

- _Instalar Make._

    abrir terminal y escriba el siguiente codigo:
    $sudo apt install make


***Problematica***

Realizar un programa que ordene con el metodo de seleccion y el metodo quicksort una lista de n numeros aleatorios entre 1 y 1000000, ademas se debe mostrar en pantalla el tiempo empleado por cada metodo y imprimir el contenido de la lista si el usuario lo desea.

***Ejecutando***

- Para ejecutar el programa debe tener todos los archivos de este repositorio en una misma carpeta, depues en la terminal usar el comando: **$_make_**, esperar que compile y ejecutar el programa con el siguiente comando: **_$./programa_**, ademas debe entregar dos parametros por terminal separados de un espacio, el primero debe ser un numero, el cual va a ser el tamaño de la lista, y el segundo debe ser un caracter, 'S' o 's' si debea que se impriman en pantalla la lista o una 'N' o 'n' si no desea imprimir la lista, dos ejemplos de como se deberia hacer es: **_$./programa 10 s_** ó **_$./programa 10 n_** 

Al ejecutar el programa, si ingreso como segundo parametro una 's' o 'S', se mostrara en pantalla la lista original con n numeros aleatorios, despues se imprimira una tabla que contiene el tiempo empleado por ambos metodos, posteriormente se imprime las listas que resultaron despues de ordenarlas ya sea por el metodo quicksort y el metodo seleccion. si ingreso como segundo parametro una 'n' o 'N', se imprimira una tabla que contiene el tiempo empleado por ambos metodos.
Si ingresa un numero menor a 1 o uno mayor a 1000000, se le pedira denuevo un numero hasta que sea entre 1 y 1000000. 

***Construido con C++***

***Librerias***

- Stdlib
- Stdio
- Iostream
- String
- Time


***Version 0.1***

***Autor***

Luis Rebolledo



