#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H
// se crea la clase
class Ordenador {
    private:
    public:
        // se genera el constructor
        Ordenador();
        //funciones
        void imprimir_vector(int a[], int n);
        void ordena_quicksort(int a[], int n);
        void ordena_seleccion(int a[], int n);
};
#endif
