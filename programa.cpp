#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "Ordenador.h"

using namespace std;

// se definen los limites para generar los aleatorios
#define LIMITE_SUPERIOR 1000000
#define LIMITE_INFERIOR 1

// se generan las listas con valores aleatorios
void generar_lista(int n, int a[], int b[]){
  int elemento;
  srand (time(NULL));
  // se van llenando las listas con numeros aleatorios entre 1 y 1000000
  for (int i=0; i<n; i++) {
    elemento = (rand() % LIMITE_SUPERIOR) + LIMITE_INFERIOR;
    a[i] = elemento;
    b[i] = elemento;
  }
}
// se imprime el cuadro con las lista original y ordenadas
void cuadro_con_listas(Ordenador o, int a[], int b[], int n){
  // se generan las variables de tiempo
  clock_t t1, t2, t3, t4;
  // se genera la variable de los milisegundos
  double secs, secs2;
  // se imprime la lista original
  cout << "Lista original:";
  o.imprimir_vector(a, n);
  // QUICKSORT

  // se toma el tiempo antes de ordenar con el metodo Quicksort
  t1 = clock();
  // se ordena con el metodo Quicksort
  o.ordena_quicksort(a, n);
  // se toma el tiempo al terminar de ordenar
  t2 = clock();
  // se calculan los milisegundos
  secs = (double)(t2 - t1) / CLOCKS_PER_SEC;

  // SELECCION
  // se toma el tiempo antes de ordenar con el metodo Seleccion
  t3 = clock();
  // se ordena la lista con el metodo se seleccion
  o.ordena_seleccion(b, n);
  // se toma el tiempo al terminar de ordenar
  t4 = clock();
  // se calculan los milisegundo
  secs2 = (double)(t4 - t3) / CLOCKS_PER_SEC;
  // se imprime el cuadro con los tiempos
  cout << "__________________________________________________" << endl;
  cout << endl;
  cout << "Metodo                   | Tiempo" << endl;
  cout << "__________________________________________________" << endl;
  cout << endl;
  cout << "Quicksort                |" << secs * 1000.0 << " milisegundos" << endl;
  cout << "Seleccion                |" << secs2 * 1000.0 << " milisegundos" << endl;
  cout << "__________________________________________________" << endl;
  cout << endl;
  // se imprimen las listas ordenadas con su metodo de ordenamiento
  cout << "Quicksort";
  o.imprimir_vector(a, n);

  cout << "Seleccion";
  o.imprimir_vector(b, n);
}

void cuadro_sin_listas(Ordenador o, int a[], int b[], int n){
  // se generan las variables de tiempo
  clock_t t1, t2, t3, t4;
  // se genera la variable de los milisegundos
  double secs, secs2;
  // se toma el tiempo antes de ordenar con el metodo Quicksort
  t1 = clock();
  // se ordena con el metodo Quicksort
  o.ordena_quicksort(a, n);
  // se toma el tiempo al terminar de ordenar
  t2 = clock();
  // se calculan los milisegundos
  secs = (double)(t2 - t1) / CLOCKS_PER_SEC;

  // SELECCION
  // se toma el tiempo antes de ordenar con el metodo Seleccion
  t3 = clock();
  // se ordena la lista con el metodo se seleccion
  o.ordena_seleccion(b, n);
  // se toma el tiempo al terminar de ordenar
  t4 = clock();
  // se calculan los milisegundo
  secs2 = (double)(t4 - t3) / CLOCKS_PER_SEC;
  // se imprime el cuadro con los tiempos
  cout << "__________________________________________________" << endl;
  cout << endl;
  cout << "Metodo                   | Tiempo" << endl;
  cout << "__________________________________________________" << endl;
  cout << endl;
  cout << "Quicksort                |" << secs * 1000.0 << " milisegundos" << endl;
  cout << "Seleccion                |" << secs2 * 1000.0 << " milisegundos" << endl;
  cout << "__________________________________________________" << endl;

}

// Main
int main(int argc, char *argv[]) {
  // Se leen los parametros por la terminal
  // atoi convierte el valor a entero
  int n = atoi(argv[1]);
  // n tiene que ser menor a 1000000 y mayor a 1, mientras eso no pase se correra el while
  while (n>1000000 or n<1) {
    // se pide un nuevo numero
    cout << "El numero que ingreso es mayor a 1000000 o menor a 0: ";
    cin >> n;
  }
  // se crea un puntero para guardar la letra
  char* ver = argv[2];
  // se crea dos listas una para cada metodo
  //QUICKSORT
  int a[n];
  //SELECCION
  int b[n];
  // se instancia la clase
  Ordenador o = Ordenador();
  // se generan las listas con numeros aleatorios
  generar_lista(n, a, b);
  // si la letra es una "s" o "S"
  if(ver[0] == 's' || ver[0] == 'S'){
    // se imprime el tiempo con las listas
    cuadro_con_listas(o, a, b, n);
  // si la letra es una "n" o "N"
  }else if(ver[0] == 'n' || ver[0] == 'N'){
    // se imprime el tiempo de cada metodo sin la lista
    cuadro_sin_listas(o, a, b, n);
  }
  return 0;
}
