#include <iostream>
#include "Ordenador.h"

using namespace std;
// se define el TRUE y FALSE
#define TRUE 0
#define FALSE 1

Ordenador::Ordenador() {}
// funcion que imprime un vector
void Ordenador::imprimir_vector(int a[], int n) {
  cout << endl;
  // recorre la lista
  for (int i=0; i<n; i++) {
    // imprime sus valores y la posicion en la que esta
    cout << "a[" << i <<"]=" << a[i] << " ";
  }
  cout << endl;
  cout << endl;
}
// funcion que ordena QUICKSORT
void Ordenador::ordena_quicksort(int a[], int n) {
  /* se toma un valor y los menores a este van a la izquierda y los mayores
  a la derecha, y eso para con todos los elemtor
  */
  int tope, ini, fin, pos;
  int pilamenor[100];
  int pilamayor[100];
  int izq, der, aux, band;

  tope = 0;
  pilamenor[tope] = 0;
  pilamayor[tope] = n-1;
  // se repite hasta que se cumpla la condicion
  while (tope >= 0) {
    // se guandan el primer valor y el segundo
    ini = pilamenor[tope];
    fin = pilamayor[tope];
    // se le resta uno al tope
    tope = tope - 1;

    // reduce
    izq = ini;
    der = fin;
    pos = ini;
    band = TRUE;
    // se repite mientras se cumpla la condicion
    while (band == TRUE) {
      while ((a[pos] <= a[der]) && (pos != der))
        der = der - 1;
        // si la posicion a la derecha es igual a la izq
      if (pos == der) {
        // band se hace falsa
        band = FALSE;
      } else {
        // sino
        // se guarada el valor en una aux y se realiza el cambio de posiciones
        aux = a[pos];
        a[pos] = a[der];
        a[der] = aux;
        pos = der;
        // se repite el ciclo mientras se cunpla la condicion
        while ((a[pos] >= a[izq]) && (pos != izq))
          // se le suma 1 a izq
          izq = izq + 1;
        // si la posicion es igual a izq
        if (pos == izq) {
          // band se hace false
          band = FALSE;
        // sino
        } else {
          // se cambian los valor
          aux = a[pos];
          a[pos] = a[izq];
          a[izq] = aux;
          pos = izq;
        }
      }
    }
    // si inicio es menor que pos - 1
    if (ini < (pos - 1)) {
      // se le suma uno al tope
      tope = tope + 1;
      // la pilamenor en la posicion tope es igual a ini
      pilamenor[tope] = ini;
      // y la pilamayor en la posicion tope es igual a pos-1
      pilamayor[tope] = pos - 1;
    }
    // si fin es mayor a pos+1
    if (fin > (pos + 1)) {
      // se le suma uno a tope
      tope = tope + 1;
      // la pilamenor en la posicion tope es igual a pos+1
      pilamenor[tope] = pos + 1;
      // y la pilamayor en la posicion tope es igual a fin
      pilamayor[tope] = fin;
    }
  }
}
// funcion que ordena SELECCION
void Ordenador::ordena_seleccion(int a[], int n) {
  /* se busca el menor dato de la lista y lo deja en la primera posicion,
  despues busca el segundo menor y lo deja en la segunda posicion... y asi sucesivamente*/
  int menor, k;

  for (int i=0; i<=n-2; i++) {
    // se guarda el valor
    menor = a[i];
    // se guarda la posicion
    k = i;
    for (int j=i+1; j<=n-1; j++) {
      // si la siguiente posicion es menor
      if (a[j] < menor) {
        // se guarda ese valor y la posicion
        menor = a[j];
        k = j;
      }
    }
    // se guardan los valores en la posicion
    a[k] = a[i];
    a[i] = menor;
  }
}
